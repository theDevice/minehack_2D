#include "../../include/requests/createAccountRequest.h"

using namespace std;

createAccountRequest::createAccountRequest( string In_Name, string In_Password )
#if USE_ENUM
: request(type_createAccountRequest)
#endif // USE_ENUM
{
    _Name = In_Name;
    _Password = In_Password;
}

createAccountRequest::~createAccountRequest(){

}



string createAccountRequest::getName(){
    return _Name;
}

string createAccountRequest::getPassword(){
    return _Password;
}


