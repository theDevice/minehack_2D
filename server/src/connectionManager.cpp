#include <string.h>
#include <sys/types.h>      //
#include <sys/socket.h>     //        = needed for the socket and server stuff
#include <netinet/in.h>     //
#include <unistd.h>         //usleep
#include <thread>



#include "../include/connectionManager.h"


using namespace std;

connectionManager::connectionManager( globalParameters* In_Parameters, view* In_View, database* In_Database, playerCharacterGenerator* In_PlayerCharacterGenerator/*!, vector<playerCharacter*>* In_AllPlayerCharacters*/ ){

    _View = In_View;
    _View->output( tab_in, __PRETTY_FUNCTION__, "entering", false );

    _Database = In_Database;

    _Parameters = In_Parameters;

    _PlayerCharacterGenerator = In_PlayerCharacterGenerator;

    _Port = In_Parameters->_Port;



    _View->output( tab_out, __PRETTY_FUNCTION__, "leaving", false );

}

connectionManager::~connectionManager(){

    for( size_t Index = 0; Index < _AllClients.size(); Index++ ){
        delete _AllClients[Index];
    }
    _AllClients.clear();

}





bool connectionManager::initSocketCommunication(){

    _View->output( tab_in, __PRETTY_FUNCTION__, "entering", false );

    bool Output = true;

    _SocketFileDescriptor = socket(AF_INET, SOCK_STREAM, 0);          //creates a new socket. It takes three arguments.
                                                        //The first is the address domain of the socket. for more see http://www.linuxhowtos.org/C_C++/socket.htm
                                                        //The second argument is the type of socket.
                                                        //The third argument is the protocol. If this argument is zero (and it always should be except for unusual circumstances), the operating system will choose the most appropriate protocol. It will choose TCP for stream sockets and UDP for datagram sockets.
                                                        //The socket system call returns an entry into the file descriptor table (i.e. a small integer). This value is used for all subsequent references to this socket. If the socket call fails, it returns -1.
    if (_SocketFileDescriptor < 0){
        _View->output( tab_stay, __PRETTY_FUNCTION__, "ERROR opening socket", false);
        Output = false;
    }
    bzero((char *) &_Server_addr, sizeof(_Client_addr));     //sets all values in a buffer to zero. It takes two arguments, the first is a pointer to the buffer and the second is the size of the buffer. Thus, this line initializes serv_addr to zeros.




        //we dont need the following because we use scanf to get the portnumber
  //  portNumber = atoi(argv[1]);                            //The port number on which the server will listen for connections is passed in as an argument, and this statement uses the atoi() function to convert this from a string of digits to an integer.

    _Server_addr.sin_family = AF_INET;                    //define the domain used. we use internet
                                                        //The variable serv_addr is a structure of type struct sockaddr_in. This structure has four fields. The first field is short sin_family, which contains a code for the address family. It should always be set to the symbolic constant AF_INET.

    _Server_addr.sin_addr.s_addr = INADDR_ANY;            //Permit any incoming IP adress by declare INADDR_ANY
                                                       //structure of type struct in_addr which contains only a single field unsigned long s_addr. This field contains the IP address of the host. For server code, this will always be the IP address of the machine on which the server is running, and there is a symbolic constant INADDR_ANY which gets this address.

    _Server_addr.sin_port = htons(_Port);                //unsigned short sin_port, which contain the port number. However, instead of simply copying the port number to this field, it is necessary to convert this to network byte order using the function htons() which converts a port number in host byte order to a port number in network byte order.



    // kill "Address already in use" error message          //http://www.unix.com/programming/29475-how-solve-error-bind-address-already-use.html
    int64_t tr=1;
    if (setsockopt(_SocketFileDescriptor,SOL_SOCKET,SO_REUSEADDR,&tr,sizeof(int)) == -1) {
        perror("setsockopt");
        exit(1);
    }



    if (bind(_SocketFileDescriptor, (struct sockaddr *) &_Server_addr,   //The bind() system call binds a socket to an address, in this case the address of the current host and port number on which the server will run. It takes three arguments, the socket file descriptor, the address to which is bound, and the size of the address to which it is bound. The second argument is a pointer to a structure of type sockaddr, but what is passed in is a structure of type sockaddr_in, and so this must be cast to the correct type. This can fail for a number of reasons, the most obvious being that this socket is already in use on this machine. The bind() manual has more information.
            sizeof(_Server_addr)) < 0){                   //
            _View->output( tab_stay, __PRETTY_FUNCTION__,"ERROR on binding", false);                 //
    }

    listen(_SocketFileDescriptor,5);                     //allows the process to listen on the socket for
                                                        //connections. The first argument is the socket file
                                                        //descriptor, and the second is the size of the
                                                        //backlog queue, i.e., the number of connections that
                                                        //can be waiting while the process is handling a
                                                        //particular connection. This should be set to 5, the
                                                        //maximum size permitted by most systems. If the first
                                                        //argument is a valid socket, this call cannot fail,
                                                        //and so the code doesn't check for errors.
                                                        //The listen() man page has more information.





    _SizeOfAdressOfClient = sizeof(_Client_addr);             // stores the size of the address of the client.
                                            //This is needed for the accept system call.
    _View->output( tab_out, __PRETTY_FUNCTION__, "leaving", false );

    return Output;
}




void connectionManager::run(){      //catches new clients and starts a new thread for each.


    _View->output( tab_in, __PRETTY_FUNCTION__, "entering", false );


    bool Success = initSocketCommunication();

    if( Success == false ){
        _View->output( tab_stay, __PRETTY_FUNCTION__, "initSocketCommunication() returned 'false'.", true );
    }

    int64_t CurrentClientID = 0;

    while( true ){

        //!usleep(2000);

        _View->output( tab_stay, __PRETTY_FUNCTION__, "_SocketFileDescriptor: " + to_string(_SocketFileDescriptor), false );
        _View->output( tab_stay, __PRETTY_FUNCTION__, "waiting for new client to connect", false );
        _View->output( tab_stay, __PRETTY_FUNCTION__, "_Port: " + to_string(_Port), false );


        // The accept() system call causes the process to block until a client connects to the server.
        // It wakes up the process when a connection from a client has been successfully established.
        // It returns a new file descriptor, and all communication on this connection should be done using the new file descriptor.
        int64_t NewSocketFileDescriptor = accept(   _SocketFileDescriptor,
                                            (struct sockaddr *) &_Client_addr,      //pointer to the address of the client on the other end of the connection, (must be cast to the correct type)
                                            &_SizeOfAdressOfClient               //size of this structure. The accept() man page has more information.
                                                                            );

        if( NewSocketFileDescriptor < 0 ){
            _View->output( tab_stay, __PRETTY_FUNCTION__, "something went veeeerryyy wrong! newSocketFileDescriptor < 0 after accepting!", false );
        }


        _AllClients.push_back( new client( _View, NewSocketFileDescriptor, CurrentClientID/*, _Database*/, _PlayerCharacterGenerator ) );



        thread startClient( &client::run, _AllClients[CurrentClientID] );

        startClient.detach();

        CurrentClientID++;

    }


    _View->output( tab_out, __PRETTY_FUNCTION__, "leaving", false );

}

