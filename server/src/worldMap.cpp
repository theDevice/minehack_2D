#include <stdlib.h>     // srand, rand

#include "../include/worldMap.h"

using namespace std;


worldMap::worldMap( mapParameters* In_Parameters, view* In_View, database* In_Database, string In_Name ){
    _View = In_View;
    _View->output( tab_in, __PRETTY_FUNCTION__, "entering", false );
    _Parameters = In_Parameters;
    _Database = In_Database;
    _Name = In_Name;
    _ChatRoom = new chatRoom( _View, _Name );

    if( _Parameters->_Type == open ){
        buildAsOpen();
        print();
    }else if( _Parameters->_Type == dungon ){
        //!missing
    }else{
        _View->output( tab_stay, __PRETTY_FUNCTION__, "unknown map-type", true );
    }

    _View->output( tab_out, __PRETTY_FUNCTION__, "leaving", false );
}

worldMap::~worldMap(){
    delete _ChatRoom;
}

void worldMap::buildAsOpen(){
    _View->output( tab_in, __PRETTY_FUNCTION__, "entering", false );

    buildEmptyMap();
    generateRandomSeedArray();

    _View->output( tab_out, __PRETTY_FUNCTION__, "leaving", false );
}

void worldMap::buildEmptyMap(){
    _View->output( tab_in, __PRETTY_FUNCTION__, "entering", false );

    position Position;
    for( Position.Y = 0; Position.Y <= _Parameters->_MaxY; Position.Y++ ){
        for( Position.X = 0; Position.X <= _Parameters->_MaxX; Position.X++ ){
            //_View->output( tab_stay, __PRETTY_FUNCTION__, "emplacing back on position: " + Position, false );
            head* NewHead = new head();
            _Map.emplace( make_pair(Position, NewHead) );
        }
    }

    _View->output( tab_out, __PRETTY_FUNCTION__, "leaving", false );
}

void worldMap::print(){
    _View->output(tab_in, __PRETTY_FUNCTION__, "entering", false);
    position Position;
    for( Position.Y = 0; Position.Y <= _Parameters->_MaxY; Position.Y++ ){
        for( Position.X = 0; Position.X <= _Parameters->_MaxX; Position.X++ ){
            string Symbol = "";
            if( _Map[Position]->_Cube == NULL ){
                Symbol = " ";
            }else{
                _View->output(tab_in, __PRETTY_FUNCTION__, "unknown Symbol", true);
            }
            _View->dump( Symbol );
        }
    }
    _View->output(tab_out, __PRETTY_FUNCTION__, "leaving", false);
}

void worldMap::generateRandomSeedArray(){
    _View->output(tab_in, __PRETTY_FUNCTION__, "entering", false);

    float ProbabilitySum = 0;
    ProbabilitySum += _Parameters->_ProbabilityOfGettingStoneSeed;
    ProbabilitySum += _Parameters->_ProbabilityOfGettingWaterSeed;
    ProbabilitySum += _Parameters->_ProbabilityOfGettingTreeSeed;
    ProbabilitySum += _Parameters->_ProbabilityOfGettingSandSeed;
    ProbabilitySum += _Parameters->_ProbabilityOfGettingGrasSeed;
    float MultiplicationFaktor = 100 / ProbabilitySum;
    _View->output( tab_stay, __PRETTY_FUNCTION__, "MultiplicationFaktor: " + to_string( MultiplicationFaktor ), false );

    int64_t _Amount_StoneSeeds = MultiplicationFaktor * _Parameters->_ProbabilityOfGettingStoneSeed;
    int64_t _Amount_WaterSeeds = MultiplicationFaktor * _Parameters->_ProbabilityOfGettingWaterSeed;
    int64_t _Amount_TreeSeeds = MultiplicationFaktor * _Parameters->_ProbabilityOfGettingTreeSeed;
    int64_t _Amount_SandSeeds = MultiplicationFaktor * _Parameters->_ProbabilityOfGettingSandSeed;
    int64_t _Amount_GrasSeeds = MultiplicationFaktor * _Parameters->_ProbabilityOfGettingGrasSeed;

    int64_t CurrentArrayIndex = 0;
    for( size_t Index = 0; Index < _Amount_StoneSeeds; Index++ ){
        RandomSeedArray[CurrentArrayIndex] = stone;
        CurrentArrayIndex++;
    }


    /*!
    srand (time(NULL));
    position Position;
    for( Position.Y = 0; Position.Y <= _Parameters->_MaxY; Position.Y++ ){
        for( Position.X = 0; Position.X <= _Parameters->_MaxX; Position.X++ ){
            if( ( (rand()%100) + 1 ) / 100 < _Parameters->_ProbabilityOfGettingASeed ){
                //! we get a seed...



            }
        }
    }
    */

    _View->output(tab_out, __PRETTY_FUNCTION__, "leaving", false);
}


cube* worldMap::getRandomCubeSeed(){
    _View->output(tab_in, __PRETTY_FUNCTION__, "entering", false);

    //! missing

    _View->output(tab_out, __PRETTY_FUNCTION__, "leaving", false);
}


bool worldMap::changePosition( const position In_OldPosition, const position In_NewPosition ){
    _View->output( tab_in, __PRETTY_FUNCTION__, "entering", false );
    _Mutex.lock();
    bool Output = false;

    if( _Map[In_NewPosition]->_Cube == NULL ){
        _Map[In_NewPosition]->_Cube = _Map[In_OldPosition]->_Cube;
        _Map[In_OldPosition]->_Cube = NULL;
        Output = true;
    }else{
        _View->output( tab_stay, __PRETTY_FUNCTION__, "couldn't move cube, because new position not NULL!!", false );
    }

    _Mutex.unlock();
    _View->output( tab_out, __PRETTY_FUNCTION__, "leaving", false );
    return Output;

}


