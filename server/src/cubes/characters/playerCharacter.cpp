
#include "../../../include/cubes/characters/playerCharacter.h"

using namespace std;

playerCharacter::playerCharacter( view* In_View, worldMap* In_WorldMap, client* In_Client, chatRoom* In_GlobalChatRoom ): character(In_View, In_WorldMap){
    _IsMassive = true;
    _GlobalChatRoom = In_GlobalChatRoom;
    _WorldMap = In_WorldMap;
}

playerCharacter::~playerCharacter(){

}

/*
action PlayerCharacter::processRequest( request In_Request ){

}
*/
