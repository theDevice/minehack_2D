

#include "../include/chatRoom.h"

chatRoom::chatRoom( view* In_View, string In_Name  ){

    _View = In_View;
    _Name = In_Name;

}


chatRoom::~chatRoom(){

}


bool chatRoom::addPlayerCharacter( playerCharacter* In_PlayerCharacter ){
    _View->output( tab_in, __PRETTY_FUNCTION__, "entering", false );
    _Mutex.lock();

    //checking if Character is already in chatRoom:
    bool AlreadyIn = false;
    bool Output = false;
    for( size_t Index = 0; Index < _Members.size(); Index++ ){
        if( _Members[Index] == In_PlayerCharacter ){
            AlreadyIn = true;
        }
    }

    if( AlreadyIn == false ){
        _Members.push_back( In_PlayerCharacter );
        Output = true;
    }else{
        _View->output( tab_stay, __PRETTY_FUNCTION__, "Player already in Chatroom!", false );
        Output = false;
    }

    _Mutex.unlock();
    _View->output( tab_out, __PRETTY_FUNCTION__, "leaving", false );
    return Output;
}


void chatRoom::removePlayerCharacter( playerCharacter* In_PlayerCharacter ){        // i know, this function is ugly as hell..
    _View->output( tab_in, __PRETTY_FUNCTION__, "entering", false );
    _Mutex.lock();

    int IndexToRemove = 0;
    bool Found = false;
    for( size_t Index = 0; Index < _Members.size() && Found == false; Index++ ){
        if( _Members[Index] == In_PlayerCharacter ){
            IndexToRemove = Index;
            Found = true;
        }
    }
    _Members.erase( _Members.begin() + IndexToRemove );

    _Mutex.unlock();
    _View->output( tab_out, __PRETTY_FUNCTION__, "leaving", false );
}
