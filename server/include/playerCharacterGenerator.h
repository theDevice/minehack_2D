#ifndef PLAYERCHARACTERGENERATOR_H
#define PLAYERCHARACTERGENERATOR_H

#include "./cubes/characters/playerCharacter.h"

#include "./database.h"

using namespace std;

class playerCharacterGenerator{

    public:
      playerCharacterGenerator( view* In_View, database* In_Database, worldMap* In_EntrenceMap, vector< playerCharacter* >* In_AllPlayerCharacters, chatRoom* In_GlobalChatRoom );
      ~playerCharacterGenerator();

      playerCharacter* getNewPlayerCharacter( string In_Name, string In_Password, client* In_Client );

    protected:

    private:
        database* _Database = NULL;
        view* _View = NULL;
        worldMap* _EntrenceMap = NULL;
        vector< playerCharacter* >* _AllPlayerCharacters = NULL;
        chatRoom* _GlobalChatroom;
};



#endif // PLAYERCHARACTERGENERATOR_H
