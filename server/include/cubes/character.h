#ifndef CHARACTER_H
#define CHARACTER_H

#include <string>

#include "../cube.h"

using namespace std;

class client;

#include "../requests/moveRequest.h"
//class moveRequest;

class character: public cube{

    public:
        character( view* In_View, worldMap* In_WorldMap );
        virtual ~character();

        void processMoveRequest( moveRequest* In_MoveRequest );

    protected:
        string _Name = "";

        //! attributes:
        int _Speed = 0;
        int _Strength = 0;
        int _Intelligence = 0;
        int _Luck = 0;


    private:



};




#endif // CHARACTER_H
